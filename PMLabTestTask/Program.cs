﻿using System;
using System.IO;
using System.Reflection;
using System.Text;

namespace PMLabTestTask
{
    internal static class Program
    {
        private static void Main()
        {
            var encoding = Encoding.ASCII;
            
            string content = GeTextContent("PMLabTestTask.Resources.mess.txt", encoding);

            string secret = MessDecoder.RevealSecret(content, minFrequencyLimit: 1, maxFrequencyLimit: 9, encoding);

            Console.WriteLine($"Secret phrase: '{secret}'");
        }

        private static string GeTextContent(string resourcePath, Encoding encoding)
        {
            var assembly = Assembly.GetExecutingAssembly();

            using (var stream = assembly.GetManifestResourceStream(resourcePath))
            using (var reader = new StreamReader(stream, encoding))
            {
                return reader.ReadToEnd();
            }
        }
    }
}