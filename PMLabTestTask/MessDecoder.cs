using System;
using System.Text;

namespace PMLabTestTask
{
    internal static class MessDecoder
    {
        public static string RevealSecret(string content, int minFrequencyLimit, int maxFrequencyLimit, Encoding encoding)
        {
            switch (encoding)
            {
                case ASCIIEncoding _:
                    return RevealSecretAscii(content, minFrequencyLimit, maxFrequencyLimit);
                default:
                    throw new ArgumentOutOfRangeException(nameof(encoding));
            }
        }

        private static string RevealSecretAscii(string content, int minFrequencyLimit, int maxFrequencyLimit)
        {
            const byte maxCharCode = 127;
            var frequencies = new int[maxCharCode + 1];

            foreach (char c in content)
                frequencies[c]++;

            var sb = new StringBuilder();

            foreach (char c in content)
            {
                if (frequencies[c] >= minFrequencyLimit &&
                    frequencies[c] <= maxFrequencyLimit)
                {
                    sb.Append(c);
                }
            }

            return sb.ToString();
        }
    }
}